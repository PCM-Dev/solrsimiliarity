package com.pcmall.solr.relevancy;

import org.apache.lucene.search.similarities.DefaultSimilarity;

public class MallSimilarity extends DefaultSimilarity {
   private static final long serialVersionUID = 4548431168039121861L;

   public float tf(float freq) {
      return freq <= 1.0F ? freq : 1.0F;
   }
}
    
